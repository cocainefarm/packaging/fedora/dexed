%global commit0 02661e7551dcf4682c4b58124a524bcb57a562ba
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global commitdate 20200207

Name:           dexed
Version:        0.9.4
Release:        %{commitdate}git%{shortcommit0}%{?dist}
Summary:        DX7 FM multi plaform/multi format plugin

License:        GPLv3.0 and ASLv2.0
URL:            https://github.com/asb2m10/dexed/
Source0:        https://github.com/asb2m10/%{name}/archive/%{commit0}/%{name}-%{shortcommit0}.tar.gz

BuildRequires:  g++
BuildRequires:  make
BuildRequires:  pkgconfig(xinerama)
BuildRequires:  pkgconfig(xrandr)
BuildRequires:  pkgconfig(xcursor)
BuildRequires:  pkgconfig(alsa)
BuildRequires:  pkgconfig(freetype2)
BuildRequires:  pkgconfig(libcurl)

%description
Please see Dexed User Website for user and download information.

Dexed is a multi platform, multi format plugin synth that is closely modeled
on the Yamaha DX7. Under the hood it uses music-synthesizer-for-android for
the synth engine and JUCE as an application/plugin wrapper.

The goal of this project is to be a tool/companion for the original DX7.
Sound engine with 'float' value parameters, different waveform à la TX81z
would be great but anything that goes beyond the DX7 should and will be a
fork of this project. This is to keep the compatibility with the original
machine.

Dexed is licensed on the GPL v3. The msfa component (acronym for music
synthesizer for android, see msfa in the source folder) stays on the
Apache 2.0 license to able to collaborate between projects.

%package vst
Summary: DX7 FM multi plaform/multi format plugin
%description vst
The vst plugin component

%prep
%setup -q -n %{name}-%{commit0}

%build
pushd Builds/Linux
%make_build
popd

%install
pushd Builds/Linux/build
install -d %{buildroot}%{_libdir}/vst %{buildroot}%{_bindir}
install --mode 755 ./Dexed.a %{buildroot}%{_libdir}/vst
install --mode 755 ./Dexed %{buildroot}%{_bindir}
popd

%files vst
%{_bindir}/Dexed

%files
%{_libdir}/vst/Dexed.a
%license LICENSE
%doc README.md Documentation/*

%changelog
* Mon Feb 10 2020 Max Audron <audron@cocaine.farm> 0.9.4hf1-1
- new package built with tito

* Mon Feb 10 2020 Max Audron
- 
